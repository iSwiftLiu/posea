'use strict'

const assert = require('chai').assert

process.env['APP_ENV'] = 'test'
require('../lib/environment')

describe('Errors', function() {
  describe('ResourceNotFoundError', function() {
    describe('constructor', function() {
      it('should set its status property to 404', function() {
        const ResourceNotFoundError = require('../lib/errors/resource-not-found')
        assert.propertyVal(new ResourceNotFoundError(), 'status', 404)
      })
    })
  })
})
