'use strict'

const winston = require('winston')
const path = require('path')
const settings = require('./settings')

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)(),
    new (winston.transports.File)({ filename: path.join(__dirname, '..', 'logs', settings['env'] + '.log') })
  ],
  level: settings['env'] === 'production' ? 'info' : 'debug'
})

module.exports = logger
