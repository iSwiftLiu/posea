'use strict'

const normalizePort = require('./normalize-port')

var settings = {}

settings['env']      = process.env['APP_ENV'] || 'development'
settings['address']  = process.env['APP_ADDRESS'] || 'localhost'
settings['port']     = normalizePort(process.env['APP_PORT'] || '3000')
settings['app root'] = process.env['APP_ROOT'] || ''
settings['app url']  = process.env['APP_URL'] ||
  'http://' + (settings['APP_ADDRESS'] || 'localhost') +
  (settings['APP_PORT'] === '80' ? '' : ':' + settings['APP_PORT']) +
  settings['app root']

module.exports = settings
