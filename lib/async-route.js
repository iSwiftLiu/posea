'use strict'

const co = require('co')

function asyncRoute(gn) {
  return function(req, res, next) {
    co.wrap(gn)(req, res, next)
    .catch(next)
  }
}

module.exports = asyncRoute
