'use strict'

const envReader = require('node-env-file')
const path = require('path')

if (!process.env['APP_ENV']) {
  process.env['APP_ENV'] = 'development'
}

envReader(path.join(__dirname, '../config/environments/' + process.env['APP_ENV'] + '.env'), {
  verbose: false,
  overwrite: false,
  raise: false,
  logger: console
})
