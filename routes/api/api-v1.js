'use strict'

const express = require('express')
const asyncRoute = require('../../lib/async-route')
const database = require('../../lib/database')
const ResourceNotFoundError = require('../../lib/errors/resource-not-found')

var router = new express.Router()

router.get('/posts/:postId', asyncRoute(function* (req, res) {
  var postId = parseInt(req.params['postId'])
  if (isNaN(postId)) {
    throw new ResourceNotFoundError(`Post with ID '${postId}' is not found`)
  }
  var post = yield database.Post.findById(postId, {
    attributes: ['id', 'title', 'url']
  })
  if (!post) {
    throw new ResourceNotFoundError(`Post with ID '${postId}' is not found`)
  }
  res.json(post)
}))

module.exports = router
