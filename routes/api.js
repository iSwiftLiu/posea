'use strict'

const express = require('express')

var router = new express.Router()

router.use('/v1', require('./api/api-v1'))

module.exports = router
